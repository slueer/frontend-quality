import { OrderActions, OrderActionTypes } from './order.actions';

export interface State {
  items: string[];
}

export const initialState: State = {
  items: []
};

export function reducer(state = initialState, action: OrderActions): State {
  switch (action.type) {

    case OrderActionTypes.UpdateSuccess:
      return action.payload;

    default:
      return state;
  }
}
